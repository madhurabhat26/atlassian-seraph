package com.atlassian.seraph.service.rememberme;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A No Op implementation of RememberMeService
 */
public class NoopRememberMeService implements RememberMeService
{
    public static final NoopRememberMeService INSTANCE = new NoopRememberMeService();
    
    private NoopRememberMeService() 
    {
    }

    @Override
    public void addRememberMeCookie(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse, final String authenticatedUsername)
    {
    }

    @Override
    public void removeRememberMeCookie(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
    {
    }

    @Override
    public String getRememberMeCookieAuthenticatedUsername(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
    {
        return null;
    }
}
