package com.atlassian.seraph.service.rememberme;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The RememberMeService is the top level entry point for getting and setting remember me cookies
 */
public interface RememberMeService
{
    /**
     * This method can be called to try and authenticate a user name from a remember me cookie.
     * <p>
     * If the cookie is not present, its token doe not match anything or it has expired, then null will be returned and
     * any presented remember me cookie in the client will be removed.
     * <p>
     * Otherwise a user name is returned, indicating that the underlying application knows about the user
     *
     * @param httpServletRequest  the request in play
     * @param httpServletResponse the response in play
     *
     * @return a username if its known about and the remember me cookie is valid
     */
    String getRememberMeCookieAuthenticatedUsername(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse);

    /**
     * This can be called to generate and save a remember me cookie with the application and send it back to the client
     *
     * @param httpServletRequest    the request in play
     * @param httpServletResponse   the response in play
     * @param authenticatedUsername the name of the user to generate the remember me cookie for
     */
    void addRememberMeCookie(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String authenticatedUsername);

    /**
     * This will remove any remember me cookie that may have been presented by the client
     *
     * @param httpServletRequest  the request in play
     * @param httpServletResponse the response in play
     */
    void removeRememberMeCookie(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse);
}
