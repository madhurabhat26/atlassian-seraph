package com.atlassian.seraph.service.rememberme;

/**
 * This interface describes the information about a remember me token
 */
public interface RememberMeToken
{
    /**
     * @return the id of this auth token.  Can be null during initial generation
     */
    Long getId();

    /**
     * @return a base64 encoded random string.  MUST NEVER be null!!
     */
    String getRandomString();

    /**
     * @return The user name associated with this token
     */
    String getUserName();

    /**
     * @return the time at which this token was created
     */
    long getCreatedTime();
}
