package com.atlassian.seraph.service.rememberme;

/**
 * This can generate secure and very random {@link RememberMeToken} objects
 */
public interface RememberMeTokenGenerator
{
    /**
     * The {@link RememberMeToken} returned will have the user name and random string set
     *
     * @param userName the user name to generate the token for
     *
     * @return a RememberMeToken  filled out with a user name and random string, ready for saving in the application
     */
    RememberMeToken generateToken(String userName);
}
