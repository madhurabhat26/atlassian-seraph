package com.atlassian.seraph.service;

import com.atlassian.seraph.SecurityService;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.util.CachedPathMapper;
import com.atlassian.seraph.util.XMLUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.opensymphony.util.ClassLoaderUtil;

import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Configures Seraph to require certain roles to access certain URL paths.
 * <p>
 * Single init-param 'config.file' which is the location of the XML config file. Default value is '/seraph-paths.xml' (loaded from classpath - usually
 * in /WEB-INF/classes)
 * <p>
 * Here's a sample of the XML config file. Path names must be unique
 * <p>
 *
 * <pre>
 * &lt;seraph-paths&gt;
 *     &lt;path name=&quot;admin&quot;&gt;
 *         &lt;url-pattern&gt;/secure/admin/*&lt;/url-pattern&gt;
 *         &lt;role-name&gt;administrators&lt;/role-name&gt;
 *     &lt;/path&gt;
 *     &lt;path name=&quot;secured&quot;&gt;
 *         &lt;url-pattern&gt;/secure/*&lt;/url-pattern&gt;
 *         &lt;role-name&gt;users&lt;/role-name&gt;
 *     &lt;/path&gt;
 * &lt;/seraph-paths&gt;
 * </pre>
 */
public class PathService implements SecurityService
{
    private static final Logger log = LoggerFactory.getLogger(PathService.class);
    static String CONFIG_FILE_PARAM_KEY = "config.file";

    String configFileLocation = "seraph-paths.xml";

    // maps url patterns to path names
    private final CachedPathMapper pathMapper = new CachedPathMapper();

    // maps roles to path names
    private final Map<String, String[]> paths = new ConcurrentHashMap<String, String[]>();

    /**
     * Init the service - configure it from the config file
     */
    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {
        if (params.get(CONFIG_FILE_PARAM_KEY) != null)
        {
            configFileLocation = params.get(CONFIG_FILE_PARAM_KEY);
        }

        configurePathMapper();
    }

    /**
     * Configure the path mapper
     */
    private void configurePathMapper()
    {
        try
        {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            URL fileUrl = ClassLoaderUtil.getResource(configFileLocation, this.getClass());

            if (fileUrl == null)
            {
                fileUrl = ClassLoaderUtil.getResource("/" + configFileLocation, this.getClass());
            }

            if (fileUrl == null)
            {
                return;
            }

            // Parse document
            final Document doc = factory.newDocumentBuilder().parse(fileUrl.toString());

            // Get list of actions
            final Element root = doc.getDocumentElement();
            final NodeList pathNodes = root.getElementsByTagName("path");

            final Map<String, String> pathMaps = new HashMap<String, String>();
            // Build list of views
            for (int i = 0; i < pathNodes.getLength(); i++)
            {
                final Element path = (Element) pathNodes.item(i);

                final String pathName = path.getAttribute("name");
                final String roleNames = XMLUtils.getContainedText(path, "role-name");
                final String urlPattern = XMLUtils.getContainedText(path, "url-pattern");

                if ((roleNames != null) && (urlPattern != null))
                {
                    final String[] rolesArr = parseRoles(roleNames);
                    paths.put(pathName, rolesArr);
                    pathMaps.put(pathName, urlPattern);
                }
            }
            pathMapper.set(pathMaps);
        }
        catch (final Exception ex)
        {
            log.error("Failed to configure pathMapper", ex);
        }
    }

    protected String[] parseRoles(final String roleNames)
    {
        final StringTokenizer st = new StringTokenizer(roleNames, ",; \t\n", false);
        final String[] roles = new String[st.countTokens()];
        int i = 0;
        while (st.hasMoreTokens())
        {
            roles[i] = st.nextToken();
            i++;
        }
        return roles;
    }

    @Override
    public void destroy()
    {}

    @Override
    public Set<String> getRequiredRoles(final HttpServletRequest request)
    {
        final String servletPath = request.getServletPath();
        return getRequiredRoles(servletPath);
    }

    public Set<String> getRequiredRoles(final String servletPath)
    {
        final Set<String> requiredRoles = new HashSet<String>();

        // then check path mappings first and add any required roles
        final Collection<String> constraintMatches = pathMapper.getAll(servletPath);

        if (constraintMatches == null)
        {
            throw new RuntimeException("No constraints matched for path " + servletPath);
        }
        for (final String constraint : constraintMatches)
        {
            final String[] rolesForPath = paths.get(constraint);
            for (int i = 0; i < rolesForPath.length; i++)
            {
                if (!requiredRoles.contains(rolesForPath[i])) // since requiredRoles is a set, isn't this useless?
                {
                    requiredRoles.add(rolesForPath[i]);
                }
            }
        }
        return Collections.unmodifiableSet(requiredRoles);
    }
}
