package com.atlassian.seraph.util;

import com.atlassian.seraph.RequestParameterConstants;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.atlassian.seraph.filter.SecurityFilter;

import java.io.UnsupportedEncodingException;
import java.net.IDN;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;

/**
 * Utilities for login link redirection.
 */
public class RedirectUtils
{
    private static final String HTTP_BASIC_AUTH_HEADER = "Authorization";
    private static final Pattern PATTERN_LEADING_SLASH = Pattern.compile("^(?:[\\/]+)(.*)$");
    private static final Pattern PATTERN_LEADING_DOUBLE_SLASH = Pattern.compile("^([\\/]{2,})(.*)");

    /**
     * Returns a login URL that would log the user in to access resource indicated by <code>request</code>.
     * <p>
     * For instance, if <code>request</code> is for protected path "/browse/JRA-123" and the user must login before accessing this resource, this
     * method might return "/login.jsp?os_destination=%2Fbrowse%2FJRA-123". Presumably the login.jsp page will redirect back to 'os_destination' once
     * logged in.
     * <p>
     * The returned path is derived from the <code>login.url</code> parameter in seraph-config.xml, which in the example above would be
     * "/login.jsp?os_destination={originalurl}". The '${originalurl}' token is replaced at runtime with a relative or absolute path to the original
     * resource requested by <code>request</code> ('/browse/JRA-123').
     * <p>
     * Both the returned URL and the ${originalurl} replacement URL may be absolute or root-relative, depending on whether the seraph-config.xml
     * <code>login.url</code> parameter is. This allows for redirection to external Single Sign-on(SSO) apps, which are
     * passed an absolute path to the originally requested resource.
     * <p>
     * No actual permission checks are performed to determine whether the user needs to log in to access the resource. The caller is assumed to have
     * done this before calling this method.
     *
     * @param request The original request made by the user for a resource.
     * @return A root-relative or absolute URL of a login link that would log the user in to access the resource.
     */
    public static String getLoginUrl(final HttpServletRequest request)
    {
        final SecurityConfig securityConfig = SecurityConfigFactory.getInstance();
        final String loginURL = securityConfig.getLoginURL();
        return getLoginURL(loginURL, request);
    }


    /**
     * Returns a login URL that would log the user in to access resource indicated by <code>request</code>. Identical to
     * {@link #getLoginUrl(javax.servlet.http.HttpServletRequest)}, except uses the 'link.login.url' parameter in seraph-config.xml instead of
     * 'login.url', which allows for different login pages depending on whether invoked from a link ("link.login.url") or from a servlet filter that
     * intercepted a request ("login.url").
     *
     * @see #getLoginUrl(javax.servlet.http.HttpServletRequest) for parameters, etc
     */
    public static String getLinkLoginURL(final HttpServletRequest request)
    {
        final SecurityConfig securityConfig = SecurityConfigFactory.getInstance();
        final String loginURL = securityConfig.getLinkLoginURL();
        return getLoginURL(loginURL, request);
    }

    public static String getLoginURL(String loginURL, final HttpServletRequest request)
    {
        final boolean externalLoginLink = isExternalLoginLink(loginURL);
        loginURL = replaceOriginalURL(loginURL, request, externalLoginLink);
        if (externalLoginLink)
        {
            return loginURL;
        }
        return request.getContextPath() + loginURL;
    }

    private static boolean isExternalLoginLink(final String loginURL)
    {
        return (loginURL.indexOf("://") != -1);
    }

    /**
     * Replace ${originalurl} token in a string with a URL for a Request.
     */
    private static String replaceOriginalURL(final String loginURL, final HttpServletRequest request, final boolean external)
    {
        final int i = loginURL.indexOf("${originalurl}");
        if (i != -1)
        {
            final String originalURL = getOriginalURL(request, external);
            final String osDest = request.getParameter(RequestParameterConstants.OS_DESTINATION);
            return loginURL.substring(0, i) + ((osDest != null) ? encodeUrl(osDest) : encodeUrl(originalURL)) + loginURL.substring(i + "${originalurl}".length());
        }
        return loginURL;
    }

    private static String encodeUrl(final String url)
    {
        try
        {
            return URLEncoder.encode(url, "UTF-8");
        }
        catch (final UnsupportedEncodingException e)
        {
            throw new AssertionError(e);
        }
    }

    /**
     * Recreate a URL from a Request.
     */
    private static String getOriginalURL(final HttpServletRequest request, final boolean external)
    {
        final String originalURL = (String) request.getAttribute(SecurityFilter.ORIGINAL_URL);
        if (originalURL != null)
        {
            if (external)
            {
                return getServerNameAndPath(request) + originalURL;
            }
            return originalURL;
        }

        if (external)
        {
            return request.getRequestURL() + (request.getQueryString() == null ? "" : "?" + request.getQueryString());
        }
        return request.getServletPath() + (request.getPathInfo() == null ? "" : request.getPathInfo()) + (request.getQueryString() == null ? "" : "?" + request.getQueryString());
    }

    /**
     * Reconstruct the context part of a request URL from a HttpServletRequest.
     * @param request the HttpServletRequest.
     * @return the context part of a request URL from the given HttpServletRequest.
     */
    public static String getServerNameAndPath(final HttpServletRequest request)
    {
        return getServerNameAndPath(request, false);
    }

    /**
     * Reconstruct the context part of a request URL from a HttpServletRequest.
     * @param request the HttpServletRequest.
     * @param showDefaultPortNumber If true, then we explicitly include the port number even when it is the default port.
     * @return the context part of a request URL from the given HttpServletRequest.
     */
    private static String getServerNameAndPath(HttpServletRequest request, boolean showDefaultPortNumber)
    {
        StringBuffer buf = new StringBuffer();
        buf.append(request.getScheme()).
                append("://").
                append(IDN.toASCII(request.getServerName()));
        if (showDefaultPortNumber ||
            ("http".equals(request.getScheme()) && request.getServerPort() != 80) ||
            ("https".equals(request.getScheme()) && request.getServerPort() != 443)
            )
        {
            buf.append(":").append(request.getServerPort());
        }
        buf.append(request.getContextPath());
        return buf.toString();
    }

    /**
     * Check whether the request authentication strategy is using
     * <a href="http://www.ietf.org/rfc/rfc2617.txt">HTTP Basic Authentication</a>
     *
     * @param request the current {@link HttpServletRequest}
     * @param basicAuthParameterName the name of the request parameter that sets the type of authorisation to apply
     * for the current <code>request</code>
     */
    public static boolean isBasicAuthentication(final HttpServletRequest request, final String basicAuthParameterName)
    {
        return hasHttpBasicAuthenticationRequestParameter(request, basicAuthParameterName) || hasHttpBasicAuthenticationRequestHeader(request);
    }

    /**
     * Check whether the * <a href="http://www.ietf.org/rfc/rfc2617.txt">HTTP Basic Authentication</a> header is set on
     * the request header
     *
     * @param request the current {@link HttpServletRequest}
     * @return <code>true</code> if the <code>Authorisation</code> header was set to <code>Basic</code>,
     *         <code>false</code> otherwise.
     */
    static boolean hasHttpBasicAuthenticationRequestHeader(HttpServletRequest request)
    {
        return containsIgnoreCase(request.getHeader(HTTP_BASIC_AUTH_HEADER), HttpServletRequest.BASIC_AUTH);
    }

    /**
     * Check whether the * <a href="http://www.ietf.org/rfc/rfc2617.txt">HTTP Basic Authentication</a> is set on
     * the request parameter. The value of the request parameter is case insensitive.
     *
     * @param request the current {@link HttpServletRequest}
     * @param basicAuthParameterName the name of the request parameter specifying the authentication type
     * @return <code>true</code> if the request contains the give request parameter sepcifying <code>basic</code> as the
     * authentication type, <code>false</code> otherwise.
     *
     */
    static boolean hasHttpBasicAuthenticationRequestParameter(HttpServletRequest request, String basicAuthParameterName)
    {
        // here we work on the query string to avoid request parsing in the filter
        String queryString = request.getQueryString();

        queryString = (queryString == null ? "&&" : "&" + queryString + "&"); // for easy pattern matching
        return queryString.indexOf("&" + basicAuthParameterName + "=" + HttpServletRequest.BASIC_AUTH.toLowerCase() + "&") != -1;
        /**
         * This actually seems WRONG in that it states that Basic Auth in case insensitive and SVN history tells me that we used to do
         * this case insensitive but this is NOT case insensitive.  I would change it but Sam actually wrote test for case senstivity
         * in 20008.  So I am leaving as is.
         *
         * Damn you backward compatibility...damn you to hell!
         */
    }

    /**
     * Appends the path to the context, dealing with any missing slashes properly. Does NOT resolve the path using URL resolution rules. Does NOT
     * attempt to normalise the resulting URL.
     *
     * @param context
     *            a context path as returned by HttpServletRequest.getContextPath, e.g. "/confluence". If null, it will be treated as the default
     *            context ("").
     * @param path
     *            a path to be appended to the context, e.g. "/homepage.action". If this is null, the context will be returned if it is non-null,
     *            otherwise the empty string will be returned.
     * @return a URL of the given path within the context, e.g. "/confluence/homepage.action".
     */
    public static String appendPathToContext(String context, final String path)
    {

        if (context == null)
        {
            context = "";
        }

        if (path == null)
        {
            return context;
        }

        if(PATTERN_LEADING_DOUBLE_SLASH.matcher(path).matches()) {
            return context;
        }

        try
        {
            URI pathUri = new URI(path);

            if(pathUri.getHost() != null)
            {
                return context;
            }
        }
        catch (URISyntaxException e)
        {
            return context;
        }

        final StringBuffer result = new StringBuffer(context);

        if (!context.endsWith("/"))
        {
            result.append("/");
        }

        String pathToAppend = path;
        if (pathToAppend.startsWith("/"))
        {
            final Matcher matcher = PATTERN_LEADING_SLASH.matcher(pathToAppend);
            if(matcher.matches())
            {
                pathToAppend = matcher.group(1);
            }
        }

        result.append(pathToAppend);
        return result.toString();
    }

    /*
     * copied from commons-lang 2.4
     */
    static boolean containsIgnoreCase(String str, String searchStr)
    {
        if (str == null || searchStr == null)
        {
            return false;
        }
        return contains(str.toUpperCase(), searchStr.toUpperCase());
    }

    /*
     * copied from commons-lang 2.4
     */
    static boolean contains(String str, String searchStr)
    {
        if (str == null || searchStr == null)
        {
            return false;
        }
        return str.indexOf(searchStr) >= 0;
    }

    /**
     * Tests if a given (absolute) URL is in the same context as the incoming HttpServletRequest.
     * This is useful for checking if we will allow a redirect to the given URL.
     *
     * @param url The URL.
     * @param request the incoming HttpServletRequest.
     * @return <code>true</code> if the given URL is in the same context as the incoming HttpServletRequest.
     */
    public static boolean sameContext(final String url, final HttpServletRequest request)
    {
        // build up the context from the request
        String context = getServerNameAndPath(request, false);
        if (sameContext(url, context))
        {
            return true;
        }
        // Its possible that the requested URL contains an explicit port number even though it is not required. Check for this.
        context = getServerNameAndPath(request, true);
        return sameContext(url, context);
    }
    private static boolean sameContext(final String url, String requestContext)
    {
        // Now, if the incoming context is "/jira", we want "/jira" and "/jira/whatever" to be considered the same context
        // but not "/jiranot"
        if (url.equals(requestContext))
        {
            return true;
        }
        // http://java.sun.com/javaee/5/docs/api/javax/servlet/ServletContext.html#getContextPath()
        // Note that Context path should not include a trailing '/', but we will be careful anyway
        if (!requestContext.endsWith("/"))
        {
            requestContext = requestContext + '/';
        }
        return url.startsWith(requestContext);
    }

}
