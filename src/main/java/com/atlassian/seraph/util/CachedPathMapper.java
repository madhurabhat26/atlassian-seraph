package com.atlassian.seraph.util;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Caches the results of the {@link PathMapper}
 */
public class CachedPathMapper implements IPathMapper
{
    private static final String NULL = new String("NULL");

    private final PathMapper delegate = new PathMapper();
    private final ConcurrentMap<String, String> cacheMap;
    private final ConcurrentMap<String, Collection<String>> cacheAllMap;

    /**
     * Creates a CachedPathMapper object that will cache the results of the {@link #get(String)} and the {@link #getAll(String)} calls.
     * <p>
     * Use the passed in maps for caches. The maps must be thread-safe as far as {@link Map#get(Object)} calls are concerned as gets may happen
     * concurrently. An access ordered map should be wrapped in a synchronizedMap wrapper.
     */
    public CachedPathMapper()
    {
        this(new ConcurrentHashMap<String, String>(), new ConcurrentHashMap<String, Collection<String>>());
    }

    /**
     * Creates a CachedPathMapper object that will use cacheMap to cache the results of the {@link #get(String)} calls and cacheAllMap to cache the
     * results of the {@link #getAll(String)} class.
     * <p>
     * Use the passed in maps for caches. The maps must be thread-safe as far as {@link Map#get(Object)} calls are concerned as gets may happen
     * concurrently. An access ordered map should be wrapped in a synchronizedMap wrapper.
     *
     * @param cacheMap
     *            for caching results of {@link #get(String)} calls
     * @param cacheAllMap
     *            for caching results of {@link #getAll(String)} calls
     */
    public CachedPathMapper(final ConcurrentMap<String, String> cacheMap, final ConcurrentMap<String, Collection<String>> cacheAllMap)
    {
        this.cacheMap = cacheMap;
        this.cacheAllMap = cacheAllMap;
    }

    @Override
    public String get(final String path)
    {
        // Check the cache
        String result = cacheMap.get(path);
        while (result == null)
        {
            String value = delegate.get(path);
            if (value == null)
            {
                value = NULL;
            }
            cacheMap.putIfAbsent(path, value);
            result = cacheMap.get(path);
        }
        return (result == NULL) ? null : result;
    }

    @Override
    public Collection<String> getAll(final String path)
    {
        Collection<String> result = cacheAllMap.get(path);
        // Check the cache
        while (result == null)
        {
            cacheAllMap.putIfAbsent(path, delegate.getAll(path));
            // The result for this key is cached, return the value
            result = cacheAllMap.get(path);
        }
        return result;
    }

    public void set(final Map<String, String> patterns)
    {
        for (final Map.Entry<String, String> entry2 : patterns.entrySet())
        {
            final Map.Entry<String, String> entry = entry2;
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void put(final String key, final String pattern)
    {
        // Let the delegate mapper update the patterns
        delegate.put(key, pattern);
        cacheMap.remove(key);
        cacheAllMap.remove(key);
    }

    @Override
    public String toString()
    {
        return delegate.toString();
    }
}