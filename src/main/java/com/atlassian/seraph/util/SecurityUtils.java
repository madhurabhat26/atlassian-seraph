package com.atlassian.seraph.util;

import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.config.SecurityConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.ISO_8859_1;

public class SecurityUtils
{
    private static final Logger log = LoggerFactory.getLogger(SecurityUtils.class);

    /** The Basic authorization encoding method. Includes trailing space, for convenient startsWith() checks, substring() and concatenation. */
    private static final String BASIC_AUTHZ_TYPE_PREFIX = "Basic ";

    private static final String ALREADY_FILTERED = "loginfilter.already.filtered";

    public static Authenticator getAuthenticator(ServletContext servletContext)
    {
        SecurityConfig securityConfig = (SecurityConfig) servletContext.getAttribute(SecurityConfig.STORAGE_KEY);

        if (securityConfig.getAuthenticator() == null)
        {
            log.error("ack! Authenticator is null!!!");
        }

        return securityConfig.getAuthenticator();
    }

    public static boolean isBasicAuthorizationHeader(String header)
    {
        return (header != null) && header.startsWith(BASIC_AUTHZ_TYPE_PREFIX);
    }

    /**
     * Extracts the username and password from the given header string (including the 'Basic ' prefix).
     * @param basicAuthorizationHeader the header to decode.
     * @return the credentials, or a username and password of "" if there were no credentials to decode.
     */
    public static UserPassCredentials decodeBasicAuthorizationCredentials(String basicAuthorizationHeader)
    {
        final String base64Token = basicAuthorizationHeader.substring(BASIC_AUTHZ_TYPE_PREFIX.length());
        // note: header fields are not UTF-8, but ISO Latin 1, even when base 64 encoded
        byte[] bytes = Base64.getDecoder().decode(base64Token);
        final String token = new String(bytes, ISO_8859_1);

        String userName = "";
        String password = "";

        final int delim = token.indexOf(":");

        if (delim != -1)
        {
            userName = token.substring(0, delim);
            password = token.substring(delim + 1);
        }
        return new UserPassCredentials(userName, password);
    }

    /**
     * Reverses the operation of decodeBasicAuthorizationCredentials. Mainly for unit tests, or ServletFilters faking
     * basic authorization.
     * @param username the username to encode.
     * @param password the password to encode.
     * @return the encoded credentials.
     */
    public static String encodeBasicAuthorizationCredentials(String username, String password)
    {
        byte[] bytes = (username + ":" + password).getBytes(ISO_8859_1);
        return BASIC_AUTHZ_TYPE_PREFIX + Base64.getEncoder().encodeToString(bytes);
    }

    /**
     * Disables seraph filtering
     * @param request
     * @since 2.5
     */
    public static void disableSeraphFiltering(ServletRequest request)
    {
        request.setAttribute(ALREADY_FILTERED, true);
    }

    /**
     * Checks if Seraph filtering is disabled
     * @param request
     * @return disabled
     * @since 2.5
     */
    public static boolean isSeraphFilteringDisabled(ServletRequest request)
    {
        return request.getAttribute(ALREADY_FILTERED) != null;
    }

    /**
     * User credentials including a username and a password.
     * TODO Eliminate duplication between this class and {@link com.atlassian.seraph.filter.PasswordBasedLoginFilter.UserPasswordPair}
     */
    public static class UserPassCredentials
    {
        private final String username;
        private final String password;

        public UserPassCredentials(String username, String password)
        {
            this.username = username;
            this.password = password;
        }

        public String getUsername()
        {
            return username;
        }

        public String getPassword()
        {
            return password;
        }
    }
}
