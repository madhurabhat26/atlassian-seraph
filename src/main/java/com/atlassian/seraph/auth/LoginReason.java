package com.atlassian.seraph.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * An enumeration of why a login attempt has failed
 */
public enum LoginReason
{
    /**
     * The user is not allowed to even attempt a login.  They are not allowed to by the {@link
     * com.atlassian.seraph.elevatedsecurity.ElevatedSecurityGuard}
     */
    AUTHENTICATION_DENIED,
    /**
     * The user could not be authenticated.
     */
    AUTHENTICATED_FAILED,
    /**
     * The user could not be authorised.
     */
    AUTHORISATION_FAILED,
    /**
     * This indicates that person has in fact logged "out"
     */
    OUT,
    /**
     * The login was OK
     */
    OK;

    /**
     * The name of the Header set by Seraph to indicate how the login process went 
     */
    public static final String X_SERAPH_LOGIN_REASON = "X-Seraph-LoginReason";
    /**
     * The name of the request attribute set by Seraph to indicate how the login process went
     */
    public static final String REQUEST_ATTR_NAME = LoginReason.class.getName();

    /**
     * This will stamp an attribute to the request called "com.atlassian.seraph.auth.LoginReason" to the toString() of
     * the enum and will also add an "X-Seraph-LoginReason" to this value
     * <p>
     * Once a request/response is stamped, it wont be stamped again
     *
     * @param httpServletRequest  the request
     * @param httpServletResponse the response
     *
     * @return this to give us a nice builder pattern
     */
    public LoginReason stampRequestResponse(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
    {
        if (httpServletRequest.getAttribute(REQUEST_ATTR_NAME) == null)
        {
            httpServletRequest.setAttribute(REQUEST_ATTR_NAME, this);
            /**
             * Whats this I hear say, how can there be no httpServletResponse when there is a httpServletRequest?  Its because the original makers of Seraph
             * in their INFINITE design wisdom decide that you could make a call like Authenticator.getUser(httpServletRequest) BUT that it would
             * expand our to call Authenticator.getUser(httpServletRequest, null).  FAN IN - FAN OUT !!
             *
             * So anyways its possible that you may not have a response object. depending on the weather and the call path.  So we handle it, he says shaking his head
             * and generally feeling pretty disgusted, once again, in Seraphs design or lack thereof!
             */
            if (httpServletResponse != null)
            {
                httpServletResponse.addHeader(X_SERAPH_LOGIN_REASON, this.toString());
            }
        }
        return this;
    }

    /**
     * Checks if the given request has been stamped with this login reason.
     *
     * @param httpServletRequest the request
     * @return true if the request has been stamped, false if otherwise
     * @see #stampRequestResponse(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public boolean isStamped(HttpServletRequest httpServletRequest)
    {
        return httpServletRequest.getAttribute(REQUEST_ATTR_NAME) == this;
    }
}
