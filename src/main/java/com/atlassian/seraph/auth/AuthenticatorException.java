package com.atlassian.seraph.auth;

/**
 * An exception for all Authenticator related error conditions
 *
 * @see Authenticator
 */
public class AuthenticatorException extends Exception
{
    private AuthenticationErrorType errorType;

    public AuthenticatorException()
    {
    }

    public AuthenticatorException(String s)
    {
        super(s);
    }

    public AuthenticatorException(AuthenticationErrorType errorType)
    {
        this(errorType.name());
        this.errorType = errorType;
    }

    public AuthenticationErrorType getErrorType()
    {
        return errorType;
    }

}
