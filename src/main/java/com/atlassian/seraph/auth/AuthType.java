package com.atlassian.seraph.auth;

import com.atlassian.seraph.config.SecurityConfig;

import javax.servlet.http.HttpServletRequest;

/**
 * If an authType is specified by the user that doesn't match a specific auth type as defined by this enum,
 * 'NONE' will be returned.  
 *
 * @since 2.2
 */
public enum AuthType
{
    /**
     * There has been no authentication type specified for this request. If the user has provided an invalid cookie they
     * will be allowed to view the page as an anonymous user.
     */
    NONE,

    /**
     * The user is presenting a session cookie that they expect to work. If it doesn't work they want to know that,
     * rather than silently proceeding as an anonymous user. In practice this means they should get a 401 or 403
     * if their session has expired.
     */
    COOKIE,

    /**
     * The user is either presenting HTTP BASIC Authentication credentials or wants the application to initial a
     * BASIC Auth challenge.
     */
    BASIC,

    /**
     * This is sort of like a combination of all three above. If you don't present BASIC Auth or a session cookie
     * then you will proceed anonymous. If you present one of those and they are invalid then you get a 401 or 403 instead.
     *
     * The difference between NONE and ANY is that with NONE you can *think* you are logging in but end up anonymous because
     * your session has expired. Remote API users (like scripts) don't notice this and just get different results. They would
     * prefer to get an obvious response code telling them that something isn't quite right.
     */
    ANY;

    public static final String DEFAULT_ATTRIBUTE = "os_authTypeDefault";

    public static AuthType getAuthTypeInformation(final HttpServletRequest request, final SecurityConfig config)
    {
        final String authTypeParamName = config.getAuthType();
        String authType = request.getParameter(authTypeParamName);
        if (authType == null)
        {
            authType = (String) request.getAttribute(DEFAULT_ATTRIBUTE);
        }
        if (authType == null)
        {
            return NONE;
        }
        else
        {
            try
            {
                return AuthType.valueOf(authType.toUpperCase());
            }
            catch (IllegalArgumentException e)
            {
                // If a non-valid authentication type is specified
                // do not use any authentication 
                return NONE;
            }
        }
    }
}
