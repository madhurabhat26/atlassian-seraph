package com.atlassian.seraph.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Invalidates and performs the manipulation of the session necessary to obtain a fresh JSESSIONID on Tomcat and
 * the equivalent on other containers. It copies the session content over to a new session with a new id (JSESSIONID)
 * and can exclude a given list of session attribute keys from the copy.
 *
 * See http://jira.atlassian.com/browse/JRA-16008
 *
 */
public class SessionInvalidator
{
    private static final Logger log = LoggerFactory.getLogger(SessionInvalidator.class);

    private final List<String> excludeList;

    /**
     * Create a session invalidator configured with the given exclusions.
     *
     * @param excludeList a list of session attribute keys to exclude from invalidated sessions.
     */
    public SessionInvalidator(final List<String> excludeList)
    {
        if (excludeList == null)
        {
            throw new IllegalArgumentException("excludeList must not be null");
        }
        this.excludeList = excludeList;
    }

    /**
     * If there is a session for the given request, invalidate it and create a new session, copying all attributes
     * over except those configured exclusions.
     *
     * @param httpServletRequest the request whose session should be invalidated.
     */
    public void invalidateSession(HttpServletRequest httpServletRequest)
    {
        final HttpSession session = httpServletRequest.getSession(false);
        if (session != null && !session.isNew())
        {
            if (log.isDebugEnabled())
            {
                dumpInfo(httpServletRequest, session);
            }
            final Map<String, Object> contents = getSessionContentsToKeep(session);
            try
            {
                session.invalidate();
                final HttpSession newSession = httpServletRequest.getSession(true);
                setAll(newSession, contents);
            }
            catch (IllegalStateException e)
            {
                log.warn("Couldn't invalidate for request because " + e.getMessage());
            }
        }
    }

    private static void setAll(final HttpSession dest, final Map<String, Object> source)
    {
        for (Map.Entry<String, Object> entry : source.entrySet())
        {
            dest.setAttribute(entry.getKey(), entry.getValue());
        }
    }

    private Map<String, Object> getSessionContentsToKeep(final HttpSession session)
    {
        Map<String, Object> sessionContents = new HashMap<String, Object>();
        Enumeration attributes = session.getAttributeNames();
        while (attributes.hasMoreElements())
        {
            String name = (String) attributes.nextElement();
            if (!excludeList.contains(name))
            {
                sessionContents.put(name, session.getAttribute(name));
            }
        }
        return sessionContents;
    }

    ///CLOVER:OFF
    private void dumpInfo(final HttpServletRequest httpServletRequest, final HttpSession session)
    {
        log.debug("invalidating session from request: "
                + httpServletRequest.getMethod() + " "
                + session.getId() + " "
                + httpServletRequest.getRequestURI() + " "
        );

        Enumeration attributes = session.getAttributeNames();
        while (attributes.hasMoreElements())
        {
            String name = (String) attributes.nextElement();
            log.debug("session attribute: " + name);
        }
    }
    ///CLOVER:ON
}
