package com.atlassian.seraph.controller;

import com.atlassian.seraph.config.SecurityConfig;

import java.util.Map;

/**
 * The default Seraph controller implementation will always enable security.
 *
 * @see SecurityController
 */
public class NullSecurityController implements SecurityController
{
    @Override
    public boolean isSecurityEnabled()
    {
        return true;
    }

    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {}
}
