package com.atlassian.seraph.filter;

import com.atlassian.seraph.auth.AuthenticationErrorType;

import javax.servlet.http.HttpServletRequest;

/**
 * Helper class to provide safe access to HTTP Request attributes set by the BaseLoginFilter.
 *
 * @since v2.4
 */
public final class LoginFilterRequest
{
    /**
     * Returns the authentication status code set by the LoginFilter as a request Attribute.
     * <p>
     * The possible statuses are:
     * <ul>
     * <li> BaseLoginFilter.LOGIN_SUCCESS - the login was processed, and user was logged in
     * <li> BaseLoginFilter.LOGIN_FAILURE - the login was processed, the user gave a bad username or password
     * <li> BaseLoginFilter.LOGIN_ERROR - the login was processed, an exception occurred trying to log the user in
     * <li> BaseLoginFilter.LOGIN_NOATTEMPT - the login was no processed, no form parameters existed
     * </ul>
     *
     * @param request the HttpServletRequest to retrieve the attribute from.
     * @return the authentication status code set by the Login Filter as a request Attribute.
     *
     * @see com.atlassian.seraph.filter.BaseLoginFilter#login(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public static String getAuthenticationStatus(HttpServletRequest request)
    {
        final Object authStatus = request.getAttribute(LoginFilter.OS_AUTHSTATUS_KEY);
        if (authStatus == null)
        {
            return null;
        }
        if (authStatus instanceof String)
        {
            return (String) authStatus;
        }
        else
        {
            // Should never happen
            throw new IllegalStateException("Illegal Authentication Status " + authStatus);
        }
    }

    /**
     * Returns the authentication error type set by the LoginFilter as a request Attribute.
     * <p>
     * This will only be set if authentication status is "error" ({@link com.atlassian.seraph.filter.BaseLoginFilter#LOGIN_ERROR}),
     * and is not even then guaranteed to be set by all implementations of {@link com.atlassian.seraph.filter.BaseLoginFilter}.
     * <p>
     * The original purpose of this was to indicate when a communication error occurs with a remote authentication server.
     *
     * @param request the HttpServletRequest to retrieve the attribute from.
     * @return the authentication status code set by the Login Filter as a request Attribute.
     *
     * @see com.atlassian.seraph.auth.AuthenticatorException#getErrorType()
     */
    public static AuthenticationErrorType getAuthenticationErrorType(HttpServletRequest request)
    {
        final Object errorType = request.getAttribute(LoginFilter.AUTHENTICATION_ERROR_TYPE);
        if (errorType == null)
        {
            return null;
        }
        if (errorType instanceof AuthenticationErrorType)
        {
            return (AuthenticationErrorType) errorType;
        }
        else
        {
            // Should never happen
            throw new IllegalStateException("Illegal Authentication ErrorType " + errorType);
        }
    }
}
