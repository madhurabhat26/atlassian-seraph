package com.atlassian.seraph.config;

import com.atlassian.seraph.util.RedirectUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.net.IDN;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

public class DefaultRedirectPolicy implements RedirectPolicy
{
    private static final Pattern MORE_THAN_2_LEADING_SLASHES = Pattern.compile("^///+");
    private static final Pattern PROTOCOL_PATTERN = Pattern.compile("^.*:");
    private static final Pattern PROTOCOL_PATTERN_WITH_SLASHES = Pattern.compile("^.*://");

    private boolean allowAnyUrl = false;

    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {
        // params is never allowed to be null.
        if (params == null)
        {
            throw new IllegalArgumentException("params is not allowed to be null");
        }
        // Allow applications to set allow.any.redirect.url=true for legacy behaviour
        allowAnyUrl = "true".equals(params.get("allow.any.redirect.url"));
    }

    /**
     * Returns true if we allow redirect to any URL at all.
     * By default this is false, however it may be configured to true in the Seraph config file to allow legacy behaviour.
     *
     * @return true if we allow redirect to any URL at all.
     */
    public boolean isAllowAnyUrl()
    {
        return allowAnyUrl;
    }

    /**
     * Checks if the given redirectURL is permitted.
     * <p>
     * Uses the configured redirect rules to see if we are allowed to redirect to the given URL.
     * By default, the following is allowed:
     * <ul>
     *   <li>Any relative URL</li>
     *   <li>An absolute URL to the same context path as the current incoming request</li>
     * </ul>
     * You can configure this "security-policy" in the Seraph XML config file.
     * eg:
     * <pre>
     *  &lt;redirect-policy class="com.atlassian.seraph.config.SimpleRedirectPolicy"&gt;
     *    &lt;init-param&gt;
     *      &lt;param-name&gt;allow.any.redirect.url&lt;/param-name&gt;
     *      &lt;param-value&gt;true&lt;/param-value&gt;
     *    &lt;/init-param&gt;
     *  &lt;/redirect-policy&gt;
     * </pre>
     * <p>
     *
     * @param redirectUrl Requested redirect URL to be verified.
     * @param request The current incoming request.
     * @return <code>true</code> if this redirectURL is allowed.
     */
    @Override
    public boolean allowedRedirectDestination(final String redirectUrl, final HttpServletRequest request)
    {
        // Test for total trust
        if (allowAnyUrl)
        {
            return true;
        }
        // Otherwise we use default behaviour: allow valid redirects to the same context.
        String foldedSlashes = foldLeadingSlashes(StringEscapeUtils.unescapeHtml4(redirectUrl), request.getScheme());
        URI uri;
        try
        {
            // Attempt to parse the URI
            uri = new URI(StringUtils.substringBefore(foldedSlashes, "?"));
        }
        catch (final URISyntaxException | IllegalArgumentException e)
        {
            // Invalid URI - not allowed. This stops possible header injection attacks (see SER-127)
            // but it is also good in general that if we can't parse a URI, then we can't trust it.
            return false;
        }
        // The URI is valid - if it is absolute, then check that it is to the same context
        return (uri.getScheme() == null && uri.getHost() == null) ||  RedirectUtils.sameContext(uri.toString(), request);
    }

    /**
     * SER-220 - browsers fold more than 2 leading slashes into just 2 of them.
     * {@link DefaultRedirectPolicy} doesn't know that trick and doesn't understand that ///google.com is handled by browsers as //google.com
     *
     * BAM-19635 - handling excessive slashes after protocol: http:///google.com is interpreted by browsers as http://google.com
     */
    private String foldLeadingSlashes(final String url, String scheme)
    {
        final Matcher protocolMatcher = PROTOCOL_PATTERN.matcher(url);
        if(protocolMatcher.find())
        {
            return protocolMatcher.group() + tryRemoveExcessSlashes(url.substring(protocolMatcher.end()), "");
        }
        else
        {
            return tryRemoveExcessSlashes(url, scheme);
        }
    }

    private String tryRemoveExcessSlashes(final String url, final String scheme)
    {
        final Matcher leadingSlashesMatcher = MORE_THAN_2_LEADING_SLASHES.matcher(url);

        if (leadingSlashesMatcher.find())
        {
            final String part = leadingSlashesMatcher.replaceFirst("//");
            return appendSchemeIfRequired(scheme, part);
        }
        else
        {
            return appendSchemeIfRequired(scheme, url);
        }
    }

    private String appendSchemeIfRequired(String scheme, String part)
    {
        if (scheme.length() == 0 || StringUtils.isNotBlank(getScheme(part)))
        {
            return part;
        }
        else if (part.startsWith(":"))
        {
            return scheme + part;
        }
        else if(part.startsWith("//"))
        {
            return scheme + ":" + part;
        }
        else
        {
            return part;
        }
    }

//    @Nullable
    private String getScheme(String url)
    {
        final Matcher protocolMatcher = PROTOCOL_PATTERN_WITH_SLASHES.matcher(url);
        if(protocolMatcher.find())
        {
            final String protocolWithSlashes = protocolMatcher.group();
            return protocolWithSlashes.substring(0, protocolWithSlashes.length() - 3);
        }
        return null;
    }
}
