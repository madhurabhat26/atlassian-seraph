package com.atlassian.seraph.config;

import com.atlassian.seraph.SecurityService;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.RoleMapper;
import com.atlassian.seraph.controller.SecurityController;
import com.atlassian.seraph.elevatedsecurity.ElevatedSecurityGuard;
import com.atlassian.seraph.interceptor.Interceptor;
import com.atlassian.seraph.service.rememberme.RememberMeService;

import java.util.List;

/**
 * Represents the configuration of Seraph.
 */
public interface SecurityConfig
{
    String STORAGE_KEY = "seraph_config";
    String BASIC_AUTH = "basic";

    List<SecurityService> getServices();

    String getLoginURL();

    String getLoginURL(boolean forUserRole, boolean forPageCaps);

    /**
     * Returns the login forward path. This is the path to <b>forward</b> to when the user tries to POST to a protected
     * resource (rather than clicking on an explicit login link). Note that this is done using a servlet FORWARD, not a
     * redirect. Information about the original request can be gotten from the <code>javax.servlet.forward.*</code>
     * request attributes.
     * <p>
     * At this point you will probably want to save the user's POST params so he can log in again and retry the POST.
     * <p>
     * Defaults to null, in which case Seraph will just do a redirect instead of a FORWARD.
     *
     * @return a String containing the login forward path, or null
     */
    String getLoginForwardPath();

    String getLinkLoginURL();

    String getLogoutURL();

    String getOriginalURLKey();

    /**
     * @return the {@link com.atlassian.seraph.auth.Authenticator} in play
     */
    Authenticator getAuthenticator();

    /**
     * @return the {@link com.atlassian.seraph.auth.AuthenticationContext} in play
     */
    AuthenticationContext getAuthenticationContext();

    /**
     * @return the {@link com.atlassian.seraph.controller.SecurityController} in play
     */
    SecurityController getController();

    /**
     * @return the {@link com.atlassian.seraph.auth.RoleMapper} in play
     */
    RoleMapper getRoleMapper();

    /**
     * @return the {@link com.atlassian.seraph.elevatedsecurity.ElevatedSecurityGuard} in play
     */
    ElevatedSecurityGuard getElevatedSecurityGuard();

    /**
     * @return the {@link com.atlassian.seraph.service.rememberme.RememberMeService} in play
     */
    RememberMeService getRememberMeService();

    /**
     * Returns the configured RedirectPolicy, or the default if none is configured.
     * Will never return null.
     * @return The configured RedirectPolicy, or the default if none is configured.
     */
    RedirectPolicy getRedirectPolicy();

    <T extends Interceptor> List<T> getInterceptors(Class<T> desiredInterceptorClass);

    void destroy();

    /**
     * @return the path that should be applied to the cookie
     */
    String getLoginCookiePath();

    /**
     * The name of the remember me cookie
     */
    String getLoginCookieKey();

    /**
     * The name of the websudo request
     */
    String getWebsudoRequestKey();

    /**
     * returns true if the remember me cookie should never be set to secure
     */
    boolean isInsecureCookie();

    /**
     * @return the maximum age of the remember me cookie
     */
    int getAutoLoginCookieAge();

    String getAuthType();

    /**
     * Whether the session (and the JSESSIONID) should be thrown away and replaced on successful login to prevent
     * session fixation.
     * @return true only if Seraph has been explicitly configured to turn on session invalidation on login.
     */
    boolean isInvalidateSessionOnLogin();

    /**
     * Whether the session (and the JSESSIONID) should be thrown away and replaced on successful websudo to prevent
     * session fixation.
     * @return true only if Seraph has been explicitly configured to turn on session invalidation on login.
     */
    boolean isInvalidateSessionOnWebsudo();

    /**
     * When sessions are invalidated upon login, sometimes there are session attributes that should be excluded from
     * the new session. List any session attribute keys here that should be excluded from the new session after
     * login.
     *
     * @return a list of session attribute keys to be excluded from the newly invalidated session.
     */
    List<String> getInvalidateSessionExcludeList();

    /**
     * When sessions are invalidated upon websudo, sometimes there are session attributes that should be excluded from
     * the new session. List any session attribute keys here that should be excluded from the new session after
     * websudo.
     *
     * @return a list of session attribute keys to be excluded from the newly invalidated session.
     */
    List<String> getInvalidateWebsudoSessionExcludeList();
}
