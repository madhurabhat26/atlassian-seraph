package com.atlassian.seraph.util;

import com.mockobjects.servlet.MockHttpServletRequest;
import com.mockobjects.ReturnValue;

import java.util.HashMap;
import java.util.Map;

/** For some reason the mockobjects wankers did not implement getRequestURL.
 * Not a hint in the comments as to why... grr
 */
public class LocalMockHttpServletRequest extends MockHttpServletRequest
{
    private final ReturnValue myRequestURL = new ReturnValue("request url");
    private int port;
    private final Map<String, Object> attributes = new HashMap<String, Object>();

    @Override
    public int getServerPort()
    {
        return port;
    }

    public void setupPort(int port)
    {
        this.port = port;
    }

    public void setupGetRequestURL(String requestURL)
    {
        myRequestURL.setValue(requestURL);
    }

    @Override
    public StringBuffer getRequestURL()
    {
        return new StringBuffer((String) myRequestURL.getValue());
    }

    @Override
    public void setAttribute(String name, Object o)
    {
        attributes.put(name, o);
    }

    @Override
    public Object getAttribute(String name)
    {
        return attributes.get(name);
    }

}
