package com.atlassian.seraph.util;

import junit.framework.TestCase;

public class TestPathMapper extends TestCase
{
    public void testRemovePath()
    {
        final PathMapper pathMapper = new PathMapper();

        pathMapper.put("foo.bar", "/foo*");
        pathMapper.put("foo.baz", "/bar*");
        assertEquals("foo.bar", pathMapper.get("/foo/bar"));
        assertEquals("foo.baz", pathMapper.get("/bar/foo"));
        pathMapper.put("foo.bar", null);
        assertNull(pathMapper.get("/foo/bar"));
        assertEquals("foo.baz", pathMapper.get("/bar/foo"));
    }
}
