package com.atlassian.seraph.util;

import com.atlassian.seraph.RequestParameterConstants;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.mockobjects.dynamic.Mock;
import com.mockobjects.servlet.MockHttpServletRequest;
import junit.framework.AssertionFailedError;
import junit.framework.TestCase;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class TestRedirectUtils extends TestCase
{
    private LocalMockHttpServletRequest mockRequest;
    private Mock mockSecurityConfig;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        // Create a project to redirect too
        mockRequest = new LocalMockHttpServletRequest();
        mockRequest.setupGetContextPath("/ctx");

        mockSecurityConfig = new Mock(SecurityConfig.class);
        SecurityConfigFactory.setSecurityConfig((SecurityConfig) mockSecurityConfig.proxy());
    }

    public void testRelativeLoginURLHasCorrectOSDestinationSet() throws IOException, ServletException
    {
        // our login.url parameter in seraph-config.xml.
        mockSecurityConfig.expectAndReturn("getLoginURL", "/login.jsp?os_destination=${originalurl}");
        mockRequest.setupGetServletPath("/browse/JRA-123");
        mockRequest.setupPathInfo(null);
        mockRequest.setupQueryString(null);
        mockRequest.setupGetAttribute(null);
        mockRequest.setupAddParameter(RequestParameterConstants.OS_DESTINATION, (String) null);
        assertEquals("/ctx/login.jsp?os_destination=%2Fbrowse%2FJRA-123", RedirectUtils.getLoginUrl(mockRequest));
    }

    public void testAbsoluteLoginURLHasCorrectOSDestinationSet() throws IOException, ServletException
    {
        // our login.url parameter in seraph-config.xml.
        mockSecurityConfig.expectAndReturn("getLoginURL", "http://sso.somecompany.com?target=${originalurl}");
        mockRequest.setupGetAttribute(null);
        mockRequest.setupQueryString(null);

        final String requestURL = "http://localhost:8080/myapp/browse/JRA-123";
        mockRequest.setupGetRequestURL(requestURL);
        mockRequest.setupAddParameter(RequestParameterConstants.OS_DESTINATION, (String) null);
        assertEquals("http://sso.somecompany.com?target=" + URLEncoder.encode(requestURL, "UTF-8"), RedirectUtils.getLoginUrl(mockRequest));
    }

    public void testRelativeLinkLoginURLHasCorrectOSDestinationSet() throws IOException, ServletException
    {
        // our login.url parameter in seraph-config.xml.
        mockSecurityConfig.expectAndReturn("getLinkLoginURL", "/login.jsp?os_destination=${originalurl}");
        mockRequest.setupGetServletPath("/browse/JRA-123");
        mockRequest.setupPathInfo(null);
        mockRequest.setupQueryString(null);
        mockRequest.setupGetAttribute(null);
        mockRequest.setupAddParameter(RequestParameterConstants.OS_DESTINATION, (String) null);
        assertEquals("/ctx/login.jsp?os_destination=%2Fbrowse%2FJRA-123", RedirectUtils.getLinkLoginURL(mockRequest));
    }

    public void testAbsoluteLinkLoginURLHasCorrectOSDestinationSet() throws IOException, ServletException
    {
        // our login.url parameter in seraph-config.xml.
        mockSecurityConfig.expectAndReturn("getLinkLoginURL", "http://sso.somecompany.com?target=${originalurl}");
        mockRequest.setupGetAttribute(null);
        mockRequest.setupQueryString(null);

        final String requestURL = "http://localhost:8080/myapp/browse/JRA-123";
        mockRequest.setupGetRequestURL(requestURL);
        mockRequest.setupAddParameter(RequestParameterConstants.OS_DESTINATION, (String) null);
        assertEquals("http://sso.somecompany.com?target=" + URLEncoder.encode(requestURL, "UTF-8"), RedirectUtils.getLinkLoginURL(mockRequest));
    }

    public void testOsDestinationKeepsOriginalValueWithLinkLoginURL() throws UnsupportedEncodingException
    {
        final String osDestParameter = "I should not be ignored";
        // our login.url parameter in seraph-config.xml.
        mockSecurityConfig.expectAndReturn("getLinkLoginURL", "/login.jsp?os_destination=${originalurl}");
        mockRequest.setupGetServletPath("/browse/JRA-123");
        mockRequest.setupPathInfo(null);
        mockRequest.setupQueryString(null);
        mockRequest.setupGetAttribute(null);
        mockRequest.setupAddParameter(RequestParameterConstants.OS_DESTINATION, osDestParameter);
        assertEquals("/ctx/login.jsp?os_destination=" + URLEncoder.encode(osDestParameter, "UTF-8"), RedirectUtils.getLinkLoginURL(mockRequest));
    }

    public void testOsDestinationKeepsOriginalValueWithLoginURL() throws UnsupportedEncodingException
    {
        final String osDestParameter = "I should not be ignored";
        // our login.url parameter in seraph-config.xml.
        mockSecurityConfig.expectAndReturn("getLoginURL", "http://sso.somecompany.com?target=${originalurl}");

        final String requestURL = "http://localhost:8080/myapp/browse/JRA-123";
        mockRequest.setupGetRequestURL(requestURL);
        mockRequest.setupQueryString(null);
        mockRequest.setupGetAttribute(null);
        mockRequest.setupAddParameter(RequestParameterConstants.OS_DESTINATION, osDestParameter);
        assertEquals("http://sso.somecompany.com?target=" + URLEncoder.encode(osDestParameter, "UTF-8"), RedirectUtils.getLoginUrl(mockRequest));
    }

    public void testAppendPathToContext()
    {
        final String path = "homepage.action";
        final String context = "/confluence";
        assertEquals("/confluence/homepage.action", RedirectUtils.appendPathToContext(context, path));
        assertEquals("/confluence/homepage.action", RedirectUtils.appendPathToContext(context + "/", path));
        assertEquals("/confluence/homepage.action", RedirectUtils.appendPathToContext(context, "/" + path));
        assertEquals("/confluence/homepage.action", RedirectUtils.appendPathToContext(context + "/", "/" + path));

        // edge cases - nulls, empty strings, etc.
        assertEquals("/homepage.action", RedirectUtils.appendPathToContext("", path));
        assertEquals("/", RedirectUtils.appendPathToContext("", ""));
        assertEquals("/confluence/", RedirectUtils.appendPathToContext(context, ""));
        assertEquals("/confluence", RedirectUtils.appendPathToContext(context, null));
        assertEquals("/homepage.action", RedirectUtils.appendPathToContext(null, path));
        assertEquals("", RedirectUtils.appendPathToContext(null, null));
    }

    public void testHasHttpBasicAuthenticationRequestHeader()
    {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        try
        {
            RedirectUtils.hasHttpBasicAuthenticationRequestHeader(request);
            fail();
        }
        catch (final AssertionFailedError e)
        {
            // expected, stupid mock that makes stupid assumptions on how it is going to be used!
        }

        request.setupAddHeader("Authorization", "");
        assertFalse(RedirectUtils.hasHttpBasicAuthenticationRequestHeader(request));

        request.setupAddHeader("Authorization", "Basic" + " some other value");
        assertTrue(RedirectUtils.hasHttpBasicAuthenticationRequestHeader(request));
    }

    public void testHasBasicAuthenticationRequestQueryParameterNullFalse()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(false, null, "testAuthParamName");
    }

    public void testHasBasicAuthenticationRequestQueryParameterDoesNot()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(false, "someparam1=somevalue1&someparam2=somevalue2", "testAuthParamName");
    }

    public void testHasBasicAuthenticationRequestQueryParameterDoesInMiddle()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(true, "someparam1=somevalue1&testAuthParamName=basic&someparam2=somevalue2", "testAuthParamName");
    }

    public void testHasNotBasicAuthenticationRequestQueryParameterInMiddleTitleCase()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(false, "someparam1=somevalue1&testAuthParamName=Basic&someparam2=somevalue2", "testAuthParamName");
    }

    public void testHasNotBasicAuthenticationRequestQueryParameterInMiddleUpperCase()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(false, "someparam1=somevalue1&testAuthParamName=BASIC&someparam2=somevalue2", "testAuthParamName");
    }

    public void testHasBasicAuthenticationRequestQueryParameterAtBeginning()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(true, "testAuthParamName=basic&someparam1=somevalue1&someparam2=somevalue2", "testAuthParamName");
    }

    public void testHasNotBasicAuthenticationRequestQueryParameterAtBeginningMixedCase()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(false, "testAuthParamName=Basic&someparam1=somevalue1&someparam2=somevalue2", "testAuthParamName");
    }

    public void testHasNotBasicAuthenticationRequestQueryParameterAtBeginningUpperCase()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(false, "testAuthParamName=BASIC&someparam1=somevalue1&someparam2=somevalue2", "testAuthParamName");
    }

    public void testHasBasicAuthenticationRequestQueryParameterAtEnd()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(true, "someparam1=somevalue1&someparam2=somevalue2&testAuthParamName=basic", "testAuthParamName");
    }

    public void testHasNotBasicAuthenticationRequestQueryParameterAtEndTitleCase()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(false, "someparam1=somevalue1&someparam2=somevalue2&testAuthParamName=Basic", "testAuthParamName");
    }

    public void testHasNotBasicAuthenticationRequestQueryParameterAtEndUpperCase()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(false, "someparam1=somevalue1&someparam2=somevalue2&testAuthParamName=BASIC", "testAuthParamName");
    }

    public void testHasNotBasicAuthenticationRequestQueryParameterPlusQueryPath()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(false, "/testAuthParamName=basic?someparam1=somevalue1&someparam2=somevalue2&testAuthParamName=BASIC", "testAuthParamName");
    }

    public void testHasNotBasicAuthenticationRequestQueryParameterPlusQueryPathAndQueryHackedUrl()
    {
        assertHasHttpBasicAuthenticationRequestQueryParameter(false, "/someUrl&testAuthParamName=basic?someparam1=somevalue1&someparam2=somevalue2&testAuthParamName=BASIC", "testAuthParamName");
    }

    private void assertHasHttpBasicAuthenticationRequestQueryParameter(final boolean does, final String query, final String parameterName)
    {
        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        request.setupQueryString(query);

        assertEquals(query, does, RedirectUtils.hasHttpBasicAuthenticationRequestParameter(request, parameterName));
    }

    //
    // Following tests have been copied from commons-lang 2.4
    //

    public void testContainsString()
    {
        assertEquals(false, RedirectUtils.contains(null, null));
        assertEquals(false, RedirectUtils.contains(null, ""));
        assertEquals(false, RedirectUtils.contains(null, "a"));
        assertEquals(false, RedirectUtils.contains("", null));
        assertEquals(true, RedirectUtils.contains("", ""));
        assertEquals(false, RedirectUtils.contains("", "a"));
        assertEquals(true, RedirectUtils.contains("abc", "a"));
        assertEquals(true, RedirectUtils.contains("abc", "b"));
        assertEquals(true, RedirectUtils.contains("abc", "c"));
        assertEquals(true, RedirectUtils.contains("abc", "abc"));
        assertEquals(false, RedirectUtils.contains("abc", "z"));
    }

    public void testContainsIgnoreCase_StringString()
    {
        assertFalse(RedirectUtils.containsIgnoreCase(null, null));

        // Null tests
        assertFalse(RedirectUtils.containsIgnoreCase(null, ""));
        assertFalse(RedirectUtils.containsIgnoreCase(null, "a"));
        assertFalse(RedirectUtils.containsIgnoreCase(null, "abc"));

        assertFalse(RedirectUtils.containsIgnoreCase("", null));
        assertFalse(RedirectUtils.containsIgnoreCase("a", null));
        assertFalse(RedirectUtils.containsIgnoreCase("abc", null));

        // Match len = 0
        assertTrue(RedirectUtils.containsIgnoreCase("", ""));
        assertTrue(RedirectUtils.containsIgnoreCase("a", ""));
        assertTrue(RedirectUtils.containsIgnoreCase("abc", ""));

        // Match len = 1
        assertFalse(RedirectUtils.containsIgnoreCase("", "a"));
        assertTrue(RedirectUtils.containsIgnoreCase("a", "a"));
        assertTrue(RedirectUtils.containsIgnoreCase("abc", "a"));
        assertFalse(RedirectUtils.containsIgnoreCase("", "A"));
        assertTrue(RedirectUtils.containsIgnoreCase("a", "A"));
        assertTrue(RedirectUtils.containsIgnoreCase("abc", "A"));

        // Match len > 1
        assertFalse(RedirectUtils.containsIgnoreCase("", "abc"));
        assertFalse(RedirectUtils.containsIgnoreCase("a", "abc"));
        assertTrue(RedirectUtils.containsIgnoreCase("xabcz", "abc"));
        assertFalse(RedirectUtils.containsIgnoreCase("", "ABC"));
        assertFalse(RedirectUtils.containsIgnoreCase("a", "ABC"));
        assertTrue(RedirectUtils.containsIgnoreCase("xabcz", "ABC"));
    }

    public void testGetServerNameAndPath() throws Exception
    {
        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        // Setup the mock "incoming" request to look like: http://example.com/jira/...
        request.setupScheme("http");
        request.setupServerName("example.com");
        request.setupPort(80);
        request.setupGetContextPath("/jira");
        assertEquals("http://example.com/jira", RedirectUtils.getServerNameAndPath(request));

        // Setup the mock "incoming" request to look like: http://example.com:88/jira/...
        request.setupScheme("http");
        request.setupServerName("example.com");
        request.setupPort(88);
        request.setupGetContextPath("/jira");
        assertEquals("http://example.com:88/jira", RedirectUtils.getServerNameAndPath(request));

        // https://example.com/jira/...
        request.setupScheme("https");
        request.setupServerName("example.com");
        request.setupPort(443);
        request.setupGetContextPath("/jira");
        assertEquals("https://example.com/jira", RedirectUtils.getServerNameAndPath(request));

        // https://example.com:8443/jira/...
        request.setupScheme("https");
        request.setupServerName("example.com");
        request.setupPort(8443);
        request.setupGetContextPath("/jira");
        assertEquals("https://example.com:8443/jira", RedirectUtils.getServerNameAndPath(request));
    }

    public void testSameContext() throws Exception
    {
        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        // Setup the mock "incoming" request to look like: http://example.com/jira/...
        request.setupScheme("http");
        request.setupServerName("example.com");
        request.setupPort(80);
        request.setupGetContextPath("/jira");
        // Same context
        assertTrue(RedirectUtils.sameContext("http://example.com/jira", request));
        assertTrue(RedirectUtils.sameContext("http://example.com/jira/", request));       
        assertTrue(RedirectUtils.sameContext("http://example.com/jira/Stuff", request));
        assertTrue(RedirectUtils.sameContext("http://example.com:80/jira/Stuff", request));
        // Different context
        assertFalse(RedirectUtils.sameContext("http://example2.com/jira", request));
        assertFalse(RedirectUtils.sameContext("http://example.com/jirax", request));
        assertFalse(RedirectUtils.sameContext("http://example2.com/jirax/", request));
        assertFalse(RedirectUtils.sameContext("http://example2.com/jirax/Stuff", request));
    }

    public void testSameContextDifferentHostNoContextPath() throws Exception
    {
        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        request.setupScheme("http");
        request.setupServerName("example.com");
        request.setupPort(80);
        request.setupGetContextPath("");
        assertFalse(RedirectUtils.sameContext("http://example.com.au", request));
    }
}
