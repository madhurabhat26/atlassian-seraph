package com.atlassian.seraph.logout;

import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;
import com.mockobjects.servlet.MockHttpServletRequest;
import com.mockobjects.servlet.MockHttpServletResponse;
import junit.framework.TestCase;

import javax.servlet.ServletException;
import java.io.IOException;

public class TestLogoutServlet extends TestCase
{
    private MockHttpServletRequest mockRequest;
    private MockHttpServletResponse mockResponse;
    Mock mockSecurityConfig;
    private LogoutServlet logoutServlet;
    private Mock mockAuthenticator;

    public TestLogoutServlet()
    {
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        // Create a project to redirect too
        mockRequest = new MockHttpServletRequest();
        mockRequest.setupGetContextPath("/ctx");
        mockResponse = new MockHttpServletResponse();
        mockSecurityConfig = new Mock(SecurityConfig.class);
        mockAuthenticator = new Mock(Authenticator.class);
        mockAuthenticator.expectAndReturn("logout", C.args(C.isA(MockHttpServletRequest.class), C.isA(MockHttpServletResponse.class)), Boolean.TRUE);
//        mockAuthenticator.expectAndReturn("logout", C.ANY_ARGS, Boolean.TRUE);
//        mockAuthenticator.expect("logout", C.args(C.isA(HttpServletRequest.class), C.isA(HttpServletResponse.class)));
        mockSecurityConfig.expectAndReturn("getAuthenticator", mockAuthenticator.proxy());
        logoutServlet = new LogoutServlet();
    }

    public void testInternalLogoutPageCausesRedirectWithNoLogoutCalled() throws IOException, ServletException
    {
        SecurityConfigFactory.setSecurityConfig((SecurityConfig) mockSecurityConfig.proxy());
        logoutServlet.init();
        mockSecurityConfig.expectAndReturn("getLogoutURL", "/logout.jsp");
        mockSecurityConfig.expectAndReturn("getLogoutURL", "/logout.jsp");
        mockResponse.setExpectedRedirect("/ctx/logout.jsp");
        logoutServlet.service(mockRequest, mockResponse);
    }

    public void testExternalLogoutPageCausesLogoutAndRedirect() throws IOException, ServletException
    {
        SecurityConfigFactory.setSecurityConfig((SecurityConfig) mockSecurityConfig.proxy());
        logoutServlet.init();
        mockRequest.setupGetContextPath("/ctx");
        mockSecurityConfig.expectAndReturn("getLogoutURL", "http://some.external.sso.page/");
        mockSecurityConfig.expectAndReturn("getLogoutURL", "http://some.external.sso.page/");
        mockResponse.setExpectedRedirect("http://some.external.sso.page/");

        logoutServlet.service(mockRequest, mockResponse);
    }

}
