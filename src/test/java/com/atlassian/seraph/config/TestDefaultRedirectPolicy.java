package com.atlassian.seraph.config;

import com.atlassian.seraph.util.LocalMockHttpServletRequest;

import java.net.IDN;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

public class TestDefaultRedirectPolicy extends TestCase
{
    private LocalMockHttpServletRequest mockJirarequest;

    @Override
    protected void setUp()
    {
        // Setup the mock "incoming" request to look like: http://example.com/jira/login.jsp
        mockJirarequest = new LocalMockHttpServletRequest();
        mockJirarequest.setupScheme("http");
        mockJirarequest.setupServerName("example.com");
        mockJirarequest.setupPort(80);
        mockJirarequest.setupGetContextPath("/jira");
    }

    public void testNullParams()
    {
        try
        {
            new DefaultRedirectPolicy().init(null, null);
            fail();
        }
        catch (final IllegalArgumentException ex)
        {
            // expected
        }
    }

    public void testNullParameter()
    {
        // initialise with an empty param map:
        final DefaultRedirectPolicy redirectPolicy = new DefaultRedirectPolicy();
        redirectPolicy.init(new HashMap<String, String>(), null);
        // Should not blow up, and should set isAllowAnyUrl=false.
        assertFalse(redirectPolicy.isAllowAnyUrl());
        // And act accordingly:
        // relative paths allowed
        assertTrue(redirectPolicy.allowedRedirectDestination("jira/admin/Stuff", mockJirarequest));
        assertTrue(redirectPolicy.allowedRedirectDestination("/jira/admin/Stuff", mockJirarequest));
        // absolute paths must stay in the same context: (http://example.com/jira/)
        assertFalse(redirectPolicy.allowedRedirectDestination("http://evil.com/jira/Stuff", mockJirarequest));
        assertTrue(redirectPolicy.allowedRedirectDestination("http://example.com/jira/Stuff", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http://example.com/crowd/Stuff", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http://example.com/jiranot", mockJirarequest));
        assertTrue(redirectPolicy.allowedRedirectDestination("http://example.com/jira", mockJirarequest));
    }

    public void testAllowAnyFalseIDN()
    {
        final String domain = IDN.toASCII("Яндекс.рф");
        final DefaultRedirectPolicy redirectPolicy = new DefaultRedirectPolicy();
        final Map<String, String> params = new HashMap<String, String>();
        params.put("allow.any.redirect.url", "false");
        mockJirarequest.setupServerName(domain);
        assertTrue(redirectPolicy.allowedRedirectDestination(
                "http://" + domain + "/jira/Stuff", mockJirarequest));
        mockJirarequest.setupServerName(IDN.toUnicode(domain));
        assertTrue(redirectPolicy.allowedRedirectDestination(
                "http://" + domain + "/jira/Stuff", mockJirarequest));
    }

    public void testAllowAnyFalse()
    {
        // initialise with "allow.any.redirect.url=false"
        final DefaultRedirectPolicy redirectPolicy = new DefaultRedirectPolicy();
        final Map<String, String> params = new HashMap<String, String>();
        params.put("allow.any.redirect.url", "false");
        redirectPolicy.init(params, null);
        // Should not blow up, and should just disallow any URLs to a different context.
        assertFalse(redirectPolicy.isAllowAnyUrl());
        // And act accordingly:

        assertTrue(redirectPolicy.allowedRedirectDestination("http://example.com/jira/Stuff", mockJirarequest));

        // relative paths allowed
        assertTrue(redirectPolicy.allowedRedirectDestination("jira/admin/Stuff", mockJirarequest));
        assertTrue(redirectPolicy.allowedRedirectDestination("/jira/admin/Stuff", mockJirarequest));
        // absolute paths must stay in the same context: (http://example.com/jira/)
        assertFalse(redirectPolicy.allowedRedirectDestination("http://evil.com/jira/Stuff", mockJirarequest));
        assertTrue(redirectPolicy.allowedRedirectDestination("http://example.com/jira/Stuff", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http://example.com/crowd/Stuff", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http://example.com/jiranot", mockJirarequest));
        assertTrue(redirectPolicy.allowedRedirectDestination("http://example.com/jira", mockJirarequest));
        assertTrue(redirectPolicy.allowedRedirectDestination("http://example.com/jira?x=|x", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http://?example.com/jira", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http://?", mockJirarequest));
        assertTrue(redirectPolicy.allowedRedirectDestination("//example.com/jira", mockJirarequest));
        assertTrue(redirectPolicy.allowedRedirectDestination("//example.co&#x6d;/jira", mockJirarequest));

        // Some testing for invalid URI's including header injection
        assertFalse(redirectPolicy.allowedRedirectDestination("jira/admin/Stuff http://elsewhere", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("jira/admin/Stuff http://elsewhere", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("jira/admin/Stuff\r\nhttp://elsewhere", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("jira/admin/Stuff\rhttp://elsewhere", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("jira/admin/Stuff\nhttp://elsewhere", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("jira/admin/Stuff http://elsewhere", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("jira/admin/Stuff\thttp://elsewhere", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("jira/admin/Stuff ", mockJirarequest));
        //SER-225
        assertFalse(redirectPolicy.allowedRedirectDestination("fTp://example.com/jira/Stuff", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("ftp://example.com/jira/Stuff", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("HtTp://example.com/jira/Stuff", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("HtTp://example.com/jira/Stuff", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("htTP://example.com/jira/Stuff", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("htTP://evil.com/jira/Stuff", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("jira/admin/Stuff HtTp://elsewhere", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("hTtP://Яндекс.рф", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("ftp://Яндекс.рф:8080/", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("FtP://Bücher.tld/", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("ftp://Bücher.tld:8080/pass.html", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("hTtPS://Bücher.tld:8080/pass.html", mockJirarequest));
        //SER-224
        assertFalse(redirectPolicy.allowedRedirectDestination("http://Яндекс.рф", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http:/Яндекс.рф", mockJirarequest));
        assertTrue(redirectPolicy.allowedRedirectDestination("/http:/Яндекс.рф", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http://Яндекс.рф/", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http://Яндекс.рф:8080/", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http://Bücher.tld", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http://Bücher.tld/", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("http://Bücher.tld/pass.html", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("https://Bücher.tld/pass.html", mockJirarequest));
        assertFalse(redirectPolicy.allowedRedirectDestination("https://Bücher.tld:8080/pass.html", mockJirarequest));
//        SER-220
        assertFalse(redirectPolicy.allowedRedirectDestination("///google.com/pass.html", mockJirarequest));

    }

    public void testAllowAnyTrue()
    {
        // initialise with "allow.any.redirect.url=true"
        final DefaultRedirectPolicy redirectPolicy = new DefaultRedirectPolicy();
        final Map<String, String> params = new HashMap<String, String>();
        params.put("allow.any.redirect.url", "true");
        redirectPolicy.init(params, null);
        // Should not blow up, and should just allow any.
        assertTrue(redirectPolicy.isAllowAnyUrl());
        assertTrue(redirectPolicy.allowedRedirectDestination(null, null));
        assertTrue(redirectPolicy.allowedRedirectDestination("", null));
        assertTrue(redirectPolicy.allowedRedirectDestination("/jira/admin/Stuff", null));
        assertTrue(redirectPolicy.allowedRedirectDestination("http://example.com", null));
        assertTrue(redirectPolicy.allowedRedirectDestination("hTtP://example.com", null));
        assertTrue(redirectPolicy.allowedRedirectDestination("//example.com", null));
        assertTrue(redirectPolicy.allowedRedirectDestination("//example.com:8080", null));
        assertTrue(redirectPolicy.allowedRedirectDestination("//example.com:8080/start.html", null));
    }

}
