package com.atlassian.seraph.service;

import com.mockobjects.servlet.MockHttpServletRequest;

import java.util.HashMap;
import java.util.Set;

import junit.framework.TestCase;

public class TestWebworkService extends TestCase
{
    public void testWithUnspecifiedActionsXmlFileParameter()
    {
        final WebworkService service = new WebworkService();
        service.init(new HashMap<String, String>(), null);
        assertRolesForURI("/AddProject.action", service, "adminUnspecified");
    }

    public void testWithSpecifiedActionsXmlFileParameter()
    {
        final WebworkService service = new WebworkService();
        final HashMap<String, String> params = new HashMap<String, String>();
        params.put("actions.xml.file", "test-actions");
        service.init(params, null);
        assertRolesForURI("/AddProject.action", service, "adminSpecified");
    }

    public void testWithSpecifiedActionExtensionsParameter()
    {
        final WebworkService service = new WebworkService();
        final HashMap<String, String> params = new HashMap<String, String>();
        params.put("actions.xml.file", "test-actions");
        params.put("action.extension", "fooity");
        service.init(params, null);
        assertRolesForURI("/AddProject.fooity", service, "adminSpecified");
    }

    private void assertRolesForURI(final String requestURI, final WebworkService service, final String role)
    {
        final MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.setupGetRequestURI(requestURI);
        final Set<String> requiredRoles = service.getRequiredRoles(servletRequest);
        assertTrue(requiredRoles.contains(role));
    }
}
