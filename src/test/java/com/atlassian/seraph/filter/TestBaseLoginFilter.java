package com.atlassian.seraph.filter;

import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.atlassian.seraph.config.SecurityConfigImpl;
import com.atlassian.seraph.util.LocalMockHttpServletRequest;
import com.atlassian.seraph.util.SecurityUtils;

import com.mockobjects.dynamic.Mock;
import com.mockobjects.servlet.MockHttpServletResponse;
import com.mockobjects.servlet.MockHttpSession;

import junit.framework.TestCase;

import java.io.IOException;
import java.security.Principal;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.mockobjects.dynamic.C.anyArgs;
import static java.util.Collections.singletonMap;

public class TestBaseLoginFilter extends TestCase
{
    private static final Principal TEST_USER = new Principal()
    {
        @Override
        public String getName()
        {
            return "Test User";
        }
    };

    private static final String SCHEME_HTTP = "http";
    private static final String SERVER_NAME_LOCALHOST = "localhost";
    private static final int SERVER_PORT = 80;
    private static final String CONTEXT_JIRA = "/jira";
    private static final String CONTEXT_ROOT = "/";

    private SecurityConfig securityConfig;
    private CountingBaseLoginFilter baseLoginFilter;

    @Override
    protected void setUp() throws Exception
    {
        baseLoginFilter = new CountingBaseLoginFilter();
    }

    public void testRedirectToOriginalDestinationNoURL() throws Exception
    {
        // Build up the mock HttpServletRequest.
        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        final HttpSession session = new MockSession();
        request.setSession(session);
        request.setupGetContextPath(CONTEXT_JIRA);
        request.setupAddParameter("os_destination", (String) null);

        // Build up the mock response.
        final Mock mockHttpServletResponse = new Mock(HttpServletResponse.class);
        // expect not to be called

        assertFalse(baseLoginFilter.redirectToOriginalDestination(request, (HttpServletResponse) mockHttpServletResponse.proxy()));

        mockHttpServletResponse.verify();
    }

    public void testRedirectToOriginalDestinationViaSession() throws Exception
    {
        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        request.setupScheme("http");
        request.setupAddParameter("os_destination", (String[]) null);
        // Set a redirect URL in the session
        final HttpSession session = new MockSession();
        session.setAttribute("seraph_originalurl", "/Stuff");
        request.setSession(session);
        request.setupGetContextPath(CONTEXT_JIRA);

        // Build up the mock response.
        final Mock mockHttpServletResponse = new Mock(HttpServletResponse.class);
        // expect to be able to redirect to given URL
        mockHttpServletResponse.expect("sendRedirect", "/jira/Stuff");

        assertTrue(baseLoginFilter.redirectToOriginalDestination(request, (HttpServletResponse) mockHttpServletResponse.proxy()));

        mockHttpServletResponse.verify();
    }

    public void testRedirectToOriginalDestinationViaRequestParameter() throws Exception
    {
        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        // No redirect URL in the session
        request.setSession(new MockSession());
        request.setupScheme("http");
        request.setupServerName("example.com");
        request.setupPort(80);
        request.setupGetContextPath(CONTEXT_JIRA);
        request.setupAddParameter("os_destination", "http://example.com/jira/Stuff");

        // Build up the mock response.
        final Mock mockHttpServletResponse = new Mock(HttpServletResponse.class);
        // expect to be able to redirect to given URL
        mockHttpServletResponse.expect("sendRedirect", "http://example.com/jira/Stuff");

        assertTrue(baseLoginFilter.redirectToOriginalDestination(request, (HttpServletResponse) mockHttpServletResponse.proxy()));

        mockHttpServletResponse.verify();
    }

    public void testRedirectToOriginalDestinationViaRequestButWhenSessionParameterIsAlsoPresent() throws Exception
    {
        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        request.setupScheme("http");
        request.setupAddParameter("os_destination", "/Request");
        // Set a redirect URL in the session
        final HttpSession session = new MockSession();
        session.setAttribute("seraph_originalurl", "/Session");
        request.setSession(session);
        request.setupGetContextPath(CONTEXT_JIRA);

        // Build up the mock response.
        final Mock mockHttpServletResponse = new Mock(HttpServletResponse.class);
        // expect to be able to redirect to given URL
        mockHttpServletResponse.expect("sendRedirect", "/jira/Request");

        assertTrue(baseLoginFilter.redirectToOriginalDestination(request, (HttpServletResponse) mockHttpServletResponse.proxy()));

        assertNull(session.getAttribute("seraph_originalurl"));
        mockHttpServletResponse.verify();
    }


    public void testRedirectToOriginalDestinationViaBoth() throws Exception
    {
        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        // No redirect URL in the session
        request.setSession(new MockSession());
        request.setupScheme("http");
        request.setupServerName("example.com");
        request.setupGetContextPath(CONTEXT_JIRA);
        request.setupAddParameter("os_destination", "http://evil.com/jira/Stuff");

        // Build up the mock response.
        final Mock mockHttpServletResponse = new Mock(HttpServletResponse.class);
        // expect to be NOT able to redirect to given URL (security violation) - we send you to the home of the context instead.
        mockHttpServletResponse.expect("sendRedirect", "/jira/");

        assertTrue(baseLoginFilter.redirectToOriginalDestination(request, (HttpServletResponse) mockHttpServletResponse.proxy()));

        mockHttpServletResponse.verify();
    }

    public void testSecurityWrappingAndLoginCalled() throws IOException, ServletException
    {
        securityConfig = new MockSecurityConfig(null, null, null, Collections.EMPTY_LIST);

        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        request.setupGetAttribute(null);
        request.setupGetParameterMap(Collections.EMPTY_MAP);

        final MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();

        CountingFilterChain filterChain = new CountingFilterChain();

        baseLoginFilter.doFilter(request, mockHttpServletResponse, filterChain);
        assertEquals(1, baseLoginFilter.getLoginCount());
        assertEquals("We must not call the FilterChain more than once", 1, filterChain.getFilterCount());
    }

    public void testInvalidateSessionOnWebsudo() throws Exception
    {
        securityConfig = new SecurityConfigImpl("test-seraph-config-with-websudo.xml");
        SecurityConfigFactory.setSecurityConfig(securityConfig);

        final HttpSession session = new MockSession();
        session.setAttribute("ASESSIONID", "session1234");
        session.setAttribute("userdata", "helloworld");

        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
//        // to indicate the user has logged in
        SecurityUtils.disableSeraphFiltering(request);
        // to indicate a websudo is in progress
        request.setAttribute("seraph.request.key", true);

        request.setSession(session);

        final MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
        CountingFilterChain filterChain = new CountingFilterChain();

        baseLoginFilter.doFilter(request, mockHttpServletResponse, filterChain);

        // The purpose of invalidating the session is to get a new session ID
        // It is tricky because session ID is not immediately created after invalidating
        // the session, hence all there is to check is the old session id no longer exist
        assertEquals("helloworld", session.getAttribute("userdata"));
        assertNotSame("session1234", session.getAttribute("ASESSIONID"));
    }

    public void testAlreadyFilterNeverCallsLogin() throws IOException, ServletException
    {
        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        SecurityUtils.disableSeraphFiltering(request);


        final MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
        CountingFilterChain filterChain = new CountingFilterChain();

        baseLoginFilter.doFilter(request, mockHttpServletResponse, filterChain);
        assertEquals(0, baseLoginFilter.getLoginCount());
        assertEquals("We must not call the FilterChain more than once", 1, filterChain.getFilterCount());
    }

    public void testRedirectIfUserIsAlreadyLoggedIn() throws IOException, ServletException
    {
        MockHttpSession session = new MockHttpSession();
        session.setupGetAttribute("seraph_originalurl", null);

        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        request.setupScheme("http");
        request.setupServerName("example.com");
        request.setupGetContextPath(CONTEXT_JIRA);
        request.setupGetParameterMap(singletonMap("os_destination", "http://example.com/jira/Stuff"));
        request.setupAddParameter("os_destination", "http://example.com/jira/Stuff");
        request.setSession(session);

        final MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
        mockHttpServletResponse.setExpectedRedirect("/jira/");

        final Mock mockAuthenticator = new Mock(Authenticator.class);
        mockAuthenticator.expectAndReturn("getUser", anyArgs(2), TEST_USER);

        securityConfig =
                new MockSecurityConfig(null, (Authenticator) mockAuthenticator.proxy(), null, Collections.EMPTY_LIST);
        SecurityConfigFactory.setSecurityConfig(securityConfig);

        CountingFilterChain filterChain = new CountingFilterChain();

        baseLoginFilter.doFilter(request, mockHttpServletResponse, filterChain);

        assertEquals(1, baseLoginFilter.getLoginCount());
        mockHttpServletResponse.verify();
        assertEquals("We must not call the FilterChain when redirecting", 0, filterChain.getFilterCount());
    }

    private void checkRedirect(String osDestination, String expectedRedirect) throws IOException
    {
        checkRedirect(osDestination, expectedRedirect, CONTEXT_ROOT);
        checkRedirect(osDestination, CONTEXT_JIRA + expectedRedirect, CONTEXT_JIRA + "/");
    }

    private void checkRedirect(String osDestination, String expectedRedirect, String context) throws IOException
    {
        final LocalMockHttpServletRequest request = new LocalMockHttpServletRequest();
        final HttpSession session = new MockSession();
        request.setupScheme(SCHEME_HTTP);
        request.setupServerName(SERVER_NAME_LOCALHOST);
        request.setupPort(SERVER_PORT);
        request.setSession(session);

        // Build up the mock response.
        final Mock mockHttpServletResponse = new Mock(HttpServletResponse.class);

        // check with root context
        request.setupGetContextPath(context);
        request.setupAddParameter("os_destination", osDestination);
        mockHttpServletResponse.expect("sendRedirect", expectedRedirect);
        baseLoginFilter.redirectToOriginalDestination(request, (HttpServletResponse) mockHttpServletResponse.proxy());
    }

    public void testRedirectToOriginalDestinationRelativeURL() throws IOException
    {
        checkRedirect("/favicon.ico", "/favicon.ico");
    }

    public void testRedirectToOriginalDestinationAbsoluteURL() throws IOException
    {
        checkRedirect("http://www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationForwardSlashes() throws IOException
    {
        checkRedirect("http:\\\\www.google.com\\favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationProtocolRelative() throws IOException
    {
        checkRedirect("//www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationProtocolRelativeWithForwardSlashes() throws IOException
    {
        checkRedirect("\\\\www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationMixedSlashes() throws IOException
    {
        checkRedirect("/\\www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationMixedSlashesRev() throws IOException
    {
        checkRedirect("\\/www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationWhiteSpacePrefix() throws IOException
    {
        checkRedirect("\r \t//www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationWhiteSpaceSuffix() throws IOException
    {
        checkRedirect("//www.google.com/favicon.ico ", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationWhiteSpaceInHostNamePath() throws IOException
    {
        checkRedirect(" //www.g\roo\ngle\n.com/fav\r\n\ticon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationWhiteSpaceBetweenSlashes() throws IOException
    {
        checkRedirect("/\n/www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationWhiteSpaceAfter() throws IOException
    {
        checkRedirect("//www.google.com/favicon.ico\n\r\u0000", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationPerc20Encoding() throws IOException
    {

        checkRedirect("%20//www.google.com/favicon.ico%20", "/%20//www.google.com/favicon.ico%20");
    }

    public void testRedirectToOriginalDestinationPerc20Encoding2() throws IOException
    {
        // Leaving this line for educational purposes
        checkRedirect("%20//www.google.com/favicon.ico", "/%20//www.google.com/favicon.ico");
    }

    public void testRedirectToOriginalDestinationNoProtocol() throws IOException
    {
        checkRedirect("www.google.com/favicon.ico", "/www.google.com/favicon.ico");
    }

    public void testRedirectToOriginalDestinationForwardSlash() throws IOException
    {
        // Leaving this line for educational purposes
        checkRedirect("www.google.com\\\\favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationMissingSlashesSameProtocol() throws IOException
    {
        checkRedirect("http:www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationMissingSlashesOtherProtocol() throws IOException
    {
        checkRedirect("https:www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationMissingSlashSameProtocol() throws IOException
    {
        // This one does not generate any risk, since browser does not correct URLs coming from headers
        checkRedirect("http:/www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationMissingSlashOtherProtocol() throws IOException
    {
        // Does not generate any risk
        checkRedirect("https:/www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationNULByteAtTheBeginning() throws IOException
    {
        // It depends on JVM version, JRE7 has improved implementation of java.net.URI
        // Although we don't support JRE6, we are still using it in OD

        if(getJavaVersion().startsWith(JVM_VERSION_6))
        {
            // Ultimately we will support JRE7+, so case for JRE6 will be gone
            // For now such result does not create any security risk.
            checkRedirect("\u0000//www.google.com/favicon.ico", "/\u0000//www.google.com/favicon.ico");
        }
        else
        {
            checkRedirect("\u0000//www.google.com/favicon.ico", CONTEXT_ROOT, CONTEXT_ROOT);
            checkRedirect("\u0000//www.google.com/favicon.ico", CONTEXT_JIRA + "/", CONTEXT_JIRA);
        }
    }

    public void testRedirectToOriginalDestinationNULByteInTheMiddle() throws IOException
    {
        checkRedirect("//www.google.com/\u0000favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationNULByteAtTheEnd() throws IOException
    {
        checkRedirect("//www.google.com/favicon.ico\u0000", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationSingleForwardSlash() throws IOException
    {
        checkRedirect("\\", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationTwoForwardSlashes() throws IOException
    {
        checkRedirect("\\\\", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationFourSlashes() throws IOException
    {
        checkRedirect("http:////google.com/favicon.ico", "/");
    }

    public void testRedirectToOriginalDestinationHTTPAuthTricks() throws IOException
    {
        checkRedirect("http:////@google.com/favicon.ico", "/");
    }

    public void testRedirectToOriginalDestinationDoubleSlashAt() throws IOException
    {
        checkRedirect("//:@", CONTEXT_ROOT, CONTEXT_ROOT);
        checkRedirect("//:@", CONTEXT_JIRA, CONTEXT_JIRA);
    }

    public void testRedirectToOriginalDestinationTripleSlash() throws IOException
    {
        checkRedirect("///www.google.com/favicon.ico", CONTEXT_ROOT, CONTEXT_ROOT);
        checkRedirect("///www.google.com/favicon.ico", CONTEXT_JIRA + "/", CONTEXT_JIRA);
    }

    public void testRedirectToOriginalDestinationPureWirdness() throws IOException
    {
        checkRedirect("///asdf@/", CONTEXT_ROOT, CONTEXT_ROOT);
        checkRedirect("///asdf@/", CONTEXT_JIRA + "/", CONTEXT_JIRA);
    }

    public void testRedirectToOriginalDestinationColonSlashSlash() throws IOException
    {
        // Even though "/://" would be a valid URL, we reject it, because it is rather a rare case
        checkRedirect("://", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationSpacesInProtocol() throws IOException
    {
        // leaving this for educational purposes
        checkRedirect("h t t p://www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    public void testRedirectToOriginalDestinationHttpAuthPlusAutocorrectionFromBrowsers() throws IOException
    {
        checkRedirect("https:anything\n:even spaces@www.google.com/favicon.ico", CONTEXT_ROOT);
    }

    class CountingBaseLoginFilter extends BaseLoginFilter
    {
        private int loginCount = 0;
        private final String loginStatus;

        public CountingBaseLoginFilter()
        {
            this(LOGIN_NOATTEMPT);
        }

        public CountingBaseLoginFilter(final String loginStatus)
        {
            this.loginStatus = loginStatus;
        }


        /**
         * We have to override this because BaseLoginFilter is abstract on this method
         *
         * @param request  the HTTP request in play
         * @param response the HTTP response in play
         *
         * @return the state of the login
         */
        @Override
        public String login(final HttpServletRequest request, final HttpServletResponse response)
        {
            loginCount++;
            return loginStatus;

        }

        @Override
        protected SecurityConfig getSecurityConfig()
        {
            if (securityConfig == null)
            {
                securityConfig = new MockSecurityConfig();
            }
            return securityConfig;
        }

        public int getLoginCount()
        {
            return loginCount;
        }
    }

    static class CountingFilterChain implements FilterChain
    {
        private int filterCount = 0;

        @Override
        public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse)
                throws IOException, ServletException
        {
            // at this stage the servlet request should be a security wrapped HttpServletRequest
            // see SER-142
            assertTrue(servletRequest instanceof BaseLoginFilter.SecurityHttpRequestWrapper);
            filterCount++;
        }

        public int getFilterCount()
        {
            return filterCount;
        }
    }

    private final static String JVM_VERSION_6 = "1.6.0";

    private static final String getJavaVersion()
    {
        return System.getProperty("java.version");
    }
}
