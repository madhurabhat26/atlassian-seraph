package com.atlassian.seraph.elevatedsecurity;

import junit.framework.TestCase;

/**
 */
public class TestNoopElevatedSecurityGuard extends TestCase
{
    public void testItAlwaysSaysTrue()
    {
        NoopElevatedSecurityGuard noopElevatedSecurityGuard = NoopElevatedSecurityGuard.INSTANCE;
        assertTrue(noopElevatedSecurityGuard.performElevatedSecurityCheck(null,null));
    }
}
