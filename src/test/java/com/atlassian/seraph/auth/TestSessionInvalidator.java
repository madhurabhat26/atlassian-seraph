package com.atlassian.seraph.auth;

import junit.framework.TestCase;
import org.mockito.Mockito;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link com.atlassian.seraph.auth.SessionInvalidator}.
 */
public class TestSessionInvalidator extends TestCase
{
    public void testInvalidateSessionNullArg()
    {
        try
        {
            SessionInvalidator si = new SessionInvalidator(null);
            fail("expected exception");
        }
        catch (RuntimeException expected)
        {

        }
    }

    public void testInvalidateSession()
    {
        SessionInvalidator si = new SessionInvalidator(Arrays.asList("monkey", "foobar"));
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);

        final MockSession session = new MockSession(false);
        session.setAttribute("monkey", "chimp");
        session.setAttribute("foobar", "baz");
        session.setAttribute("legit", "shiznit");
        session.setAttribute("groovy", "baby");

        when(mockRequest.getSession(false)).thenReturn(session);
        when(mockRequest.getMethod()).thenReturn("POST");

        final HttpSession newSession = mock(HttpSession.class, "new session");
        when(mockRequest.getSession(true)).thenReturn(newSession);

        si.invalidateSession(mockRequest);

        verify(newSession).setAttribute("legit", "shiznit");
        verify(newSession).setAttribute("groovy", "baby");
        verify(newSession, never()).setAttribute("monkey", "chimp");
        verify(newSession, never()).setAttribute("foobar", "baz");

    }

    public void testInvalidateSessionNone()
    {
        SessionInvalidator si = new SessionInvalidator(Arrays.asList("monkey", "foobar"));
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);

        final MockSession session = new MockSession(true);
        session.setAttribute("monkey", "chimp");
        session.setAttribute("foobar", "baz");
        session.setAttribute("legit", "shiznit");
        session.setAttribute("groovy", "baby");

        when(mockRequest.getSession(true)).thenReturn(session);
        when(mockRequest.getMethod()).thenReturn("POST");

        final HttpSession newSession = mock(HttpSession.class, "new session");
        when(mockRequest.getSession(true)).thenReturn(newSession);

        si.invalidateSession(mockRequest);

        verify(newSession, never()).setAttribute(Mockito.anyString(), Mockito.<Object>any());

    }
}
