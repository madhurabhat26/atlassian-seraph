# Atlassian Seraph

## Description

Seraph is a Servlet security framework for use in Java EE web applications.

## Atlassian Developer?

### Committing Guidelines

Although not part of the Atlassian Java Platform, please see
[the following guide](https://hello.atlassian.net/wiki/x/7atcC) for committing to this module.

### Builds

The Bamboo builds for this project are found [here](https://engservices-bamboo.internal.atlassian.com/browse/SER).


## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://jira.atlassian.com/browse/SER).

### Documentation

* In JIRA: [JIRA Architectural Overview - Seraph](https://developer.atlassian.com/display/JIRADEV/JIRA+Architectural+Overview#JIRAArchitecturalOverview-Seraph).
* In Confluence: [HTTP authentication with Seraph](https://developer.atlassian.com/display/CONFDEV/HTTP+authentication+with+Seraph).
